>在线体验：[http://hi.tsyj.net/hospital/sel/login](http://hi.tsyj.net/hospital/sel/login)
>admin/123456
>源码，下载要钱的推广地址：[http://www.zuidaima.com/share/4987182606945280.htm](http://www.zuidaima.com/share/4987182606945280.htm)
# 1 门诊管理
## 1.1 用户挂号
### 1.1.1 查询全部挂号列表
- 业务说明：
- 请求路径：`/hospital/cao/reportlist/all`
- 请求类型：`GET`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|-----|
|`query`|病人的姓名，需要模糊查询|不能为空|


- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	|

data中的数据

|参数名称	|参数说明|	备注|
|---:|:---|---|
|`id`	|  挂号单`id`  |  |
|`reportName`	| 病人姓名   |  |
|`phone`	| 病人手机号   |  |
|`carid`	|  病人身份证号  |  |
|`department`	|  科室名字  |  |
|`reporttype`	| 挂号类型名字   |  |
|`date`|挂号时间    |  |
|`doctorName`|医生名字    |  |

- 返回格式
```json
[
  {
    "id": 35,
    "reportName": "张翠山",
    "phone": "15555555555",
    "carid": "371326******888",
    "departmentName": "内科",
    "reporttypeName": "普通挂号",
    "doctor": "华佗",
    "date": "2021-12-24 13:18:05"
  },
  {
    "id": 36,
    "reportName": "李思思",
    "phone": "15986754321",
    "carid": "371326******111",
    "departmentName": "内科",
    "reporttypeName": "专家号",
    "doctor": "蔡伦",
    "date": "2021-12-24 13:19:02"
  }
]
```

### 1.1.2 查询当天挂号列表
- 业务说明：只返回当天挂号的数据
- 请求路径：`/hospital/cao/reportlist/day`
- 请求类型：`GET`
- 请求参数：无

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	|

data中的数据

|参数名称	|参数说明|	备注|
|---:|:---|---|
|`id`	|  挂号单`id`  |  |
|`reportName`	| 病人姓名   |  |
|`phone`	| 病人手机号   |  |
|`carid`	|  病人身份证号  |  |
|`departmentName`	|  科室名字  |  |
|`reporttypeName`	| 挂号类型名字   |  |
|`date`|挂号时间    |  |
|`doctor`|医生名字    |  |

- 返回格式
```json
[
  {
    "id": 35,
    "reportName": "张翠山",
    "phone": "15555555555",
    "carid": "371326******888",
    "departmentName": "内科",
    "reporttypeName": "普通挂号",
    "doctor": "华佗",
    "date": "2021-12-24 13:18:05"
  },
  {
    "id": 36,
    "reportName": "李思思",
    "phone": "15986754321",
    "carid": "371326******111",
    "departmentName": "内科",
    "reporttypeName": "专家号",
    "doctor": "蔡伦",
    "date": "2021-12-24 13:19:02"
  }
]
```
### 1.1.3 查看预约挂号列表
- 业务说明：只返回预约挂号的数据，由于当前全部都是当天挂号，所以这个可以先不做
- 请求路径：`/hospital/cao/reportlist/before`
- 请求类型：`GET`
- 请求参数：无

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据||

data中的数据

|参数名称	|参数说明|	备注|
|---:|:---|---|
|`id`	|  挂号单`id`  |  |
|`reportName`	| 病人姓名   |  |
|`phone`	| 病人手机号   |  |
|`carid`	|  病人身份证号  |  |
|`departmentName`	|  科室名字  |  |
|`reporttypeName`	| 挂号类型名字   |  |
|`date`|挂号时间    |  |
|`doctor`|医生名字    |  |

- 返回格式
```json
[
  {
    "id": 35,
    "reportName": "张翠山",
    "phone": "15555555555",
    "carid": "371326******888",
    "departmentName": "内科",
    "reporttypeName": "普通挂号",
    "doctor": "华佗",
    "date": "2021-12-24 13:18:05"
  },
  {
    "id": 36,
    "reportName": "李思思",
    "phone": "15986754321",
    "carid": "371326******111",
    "departmentName": "内科",
    "reporttypeName": "专家号",
    "doctor": "蔡伦",
    "date": "2021-12-24 13:19:02"
  }
]
```

### 1.1.4 查询科室
- 业务说明：
- 请求路径：`/hospital/cao/seldep`
- 请求类型：`Get`
- 请求参数：无
- 响应结果：

```json
[
  {
    "departmentId": 7,
    "department": "内科"
  },
  {
    "departmentId": 8,
    "department": "外科"
  }
]
```

### 1.1.5 查询挂号类型
- 业务说明：附带价格
- 请求路径：`/hospital/cao/selreg`
- 请求类型：`Get`
- 请求参数：无
- 响应结果：

```json
[
  {
    "registerId": 5,
    "type": "普通挂号",
    "price": 20.0
  },
  {
    "registeredId": 6,
    "type": "专家号",
    "price": 30.0
  }
]
```
### 1.1.6 查询对应科室、挂号类型下对应的医生
- 业务说明：
- 请求路径：`/hospital/cao/seldoctor`
- 请求类型：`Get`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|-----|
|`registeredid`|挂号类型的`ID`|不能为空|
|`departmentId`|科室的`ID`|不能为空|

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	|
- 返回格式
```json
[
  {
    "doctorId": 14,
    "doctorName": "扁鹊",
    "departmentId": null,
    "registeredid": null,
    "dstate": null
  }
]
```

### 1.1.7 添加挂号单，写入数据库
- 业务说明：
- 请求路径：`/hospital/cao/addre`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`reportName`|病人名字|不能为空|
|`sex`|姓别|不能为空|
|`age`|年龄|不能为空|
|`users`|用户，当前登陆的用户，如医生、护士、超级管理员|不能为空|
|`phone`|病人手机号|不能为空|
|`carid`|身份证号|不能为空|
|`department`|科室`Id`|不能为空|
|`reporttype`|挂号类型`Id`|不能为空|
|`doctorId`|医生`Id`|不能为空|
|`price`|挂号价格，这个应该是挂号时直接收取的，不需要再开窗口单独收取或向下流转了|不能为空|

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	|

### 1.1.8 删除挂号单
- 业务说明：
- 请求路径：`/hospital/cao/delre`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`id`|挂号单`id`|不能为空|


- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|可以返回行数也可以不返回	|

![输入图片说明](image.png)
![输入图片说明](images/imagefromr.png)
## 1.2 处方划价
### 1.2.1 分页查询已挂号，等待问诊的病人列表
- 业务说明：查询所有已经挂号了的、等待问诊的病人列表
- 请求路径：`/hospital/caocc/selpreson`
- 请求类型：`GET`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`durgname`|病人姓名，模糊匹配，如果没有传值，应该返回所有的记录|||
|`page`|当前页码|不能为空|
|`limit`|每页条数|不能为空|

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	|
- data中的数据

```yaml
{
  "msg": "",
  "code": 0,
  "data": [
    {
      "reportId": 35,
      "reportName": "张翠山",
      "sex": "男",
      "age": 23,
      "price": 20.0,
      "time": "2021-12-24",
      "users": "超级管理员",
      "state": 1,
      "ddepartmentid": 7,
      "ddoctorid": 13,
      "dredisteredid": null,
      "department": "内科",
      "doctorName": "华佗",
      "type": "普通挂号",
      "carid": "371326188888888888",
      "phone": "15555555555",
      "carido": null,
      "caridt": null,
      "cc": null,
      "datime": null,
      "zhuan": null
    }
  ],
  "count": 1
}
```
### 1.2.2 分页查询药品
- 业务说明：查询所有的药品，可以使用名字模糊查询
- 请求路径：`/hospital/caocc/seldrug`
- 请求类型：`GET`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`durgname`|药品的名字，模糊匹配，如果为空或没有传值 ，直接所回所有的药品||
|`page`|当前页码|不能为空|
|`limit`|每页条数|不能为空|


- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	|
- data中的数据

```yaml
{
  "msg": "",
  "code": 0,
  "data": [
    {
      "pharmacyId": 12,
      "pharmacyName": "阿莫西林",
      "drugstoreId": null,
      "skullId": 4,
      "warehouseId": 2,
      "unit": 7,
      "sellingPrice": 30.0,
      "area": 4,
      "type": 5,
      "produceDate": "2019-10-24T16:00:00.000+0000",
      "validDate": "2020-01-31T16:00:00.000+0000",
      "drugstorenum": 7,
      "skullbatch": 20191121,
      "unitname": "盒"
    },
    {
      "pharmacyId": 11,
      "pharmacyName": "板蓝根",
      "drugstoreId": null,
      "skullId": 4,
      "warehouseId": 2,
      "unit": 9,
      "sellingPrice": 30.0,
      "area": 4,
      "type": 5,
      "produceDate": "2019-10-29T16:00:00.000+0000",
      "validDate": "2020-01-03T16:00:00.000+0000",
      "drugstorenum": 57,
      "skullbatch": 20191121,
      "unitname": "袋"
    }
  ],
  "count": 2
}
```
### 1.2.3 根据`Id`查询指定病人的病因
- 业务说明：查询所有的药品，可以使用名字模糊查询
- 请求路径：`/hospital/caocc/selbing`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`reid`|病人挂号后的`id`，可以暂定为挂号单的`id`，也可以想办法实现区分本次挂号的病因和以及历史病因||


- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	直接写病因就行了，文字|
- data中的数据

```yaml
木大事
```
### 1.2.4 添加指定`Id`的病人的病因
- 业务说明：设置指定`id`的病人的病因
- 请求路径：`/hospital/caocc/addbing`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`reid`|病人挂号后的`id`，可以暂定为挂号单的`id`，也可以想办法实现区分本次挂号的病因和以及历史病因||
|`bing`|病人的病因，指本次诊断的病因，可以简单做成直接覆盖的，也可以做个带修改日志的||


- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	返回个行数也行，不返回也成|
- data中的数据

```yaml
1
```

### 1.2.5 添加指定`Id`的病人的药品
- 业务说明：
    - 给指定`id`的病人添加指定的药品。原项目中添加时会对病因做一个`check`，如果已经有了病因，则正常添加，如果还没有填写病因，也就是还没有被医生诊断过，所以是不能直接添加药品的，这个问题前端可以再想办法修改一下医生诊断界面的布局来改善各环节的流畅度
    - 如果要添加的药品已经存在了，则在原有基础上增加数量即可
- 请求路径：`/hospital/caocc/addchu`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`reportId`|挂号单的`id`||
|`pharmacyId`|药品`id`||
|`pharmacyName`|药品名字   | 按理说不应该存药品的名字的，但是如果药品后面修改了名字，再查询的时候名字就是更新的了  |
|`durgnum`|  药品数量 |   |
|`repiceprice`|药品价格   | 价格的这个后面可以想如何实现先进先出  |

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	返回个行数也行，不返回也成|
- data中的数据

```yaml
1
```
### 1.2.6 查询指定`Id`的病人的药品
- 业务说明：查询指定病人的、已经添加了的药品，就是本次诊断给病人下的药
- 请求路径：`/hospital/caocc/selall`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`perid`|药品的名字，模糊匹配，如果为空或没有传值 ，直接所回所有的药品||
|`page`|当前页码|不能为空|
|`limit`|每页条数|不能为空|

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	返回个行数也行，不返回也成|
- data中的数据

```yaml
{
  "msg": "",
  "code": 0,
  "data": [
    {
      "cashier": 57,
      "reportId": 35,
      "durgname": "阿莫西林",
      "durgnum": 1,
      "repiceprice": 30.0,
      "state": 0,
      "ostate": null,#没搞懂这个是啥
      "jie": null,   #没搞懂这个是啥
      "mstate": null #没搞懂这个是啥
    }
  ],
  "count": 1
}
```
## 1.3 项目划价
### 1.3.1 查询所有可以做的检查项目
- 业务说明：
- 请求路径：`/hospital/caoout/selout`
- 请求类型：`GET`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`projectName`|检查项目的名字，模糊匹配，如果为空或没有传值 ，直接所回所有的可做检查项目，也可以根据当前医生所属的科来筛选一下||
|`page`|当前页码|不能为空|
|`limit`|每页条数|不能为空|

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	返回个行数也行，不返回也成|
- data中的数据

```javascript
{
  "msg": "",
  "code": 0,
  "data": [
    {
      "outpatientId": 7,
      "projectName": "ct",
      "unit": 8,
      "bigprojectId": 2,
      "price": 100.0,
      "unitName": "次",
      "ostate": 1
    },
    {
      "outpatientId": 8,
      "projectName": "打针",
      "unit": 8,
      "bigprojectId": 2,
      "price": 20.0,
      "unitName": "次",
      "ostate": 0
    }
  ],
  "count": 2
}
```
### 1.3.2 给病人添加检查项目
- 业务说明：给病人添加检查项目，与本次挂号单相关。原项目中添加检查项目时会对病因做一个`check`，如果已经有了病因，则正常添加检查项目，如果还没有填写病因，也就是还没有被医生诊断过，所以是不能直接添加检查项目的，这个可以再想办法修改一下
- 请求路径：`/hospital/caoout/addchuo`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`reportId`|挂号单的`id`||
|`outpatientId`|检查项目`id`||
|`projectName`|检查项目名字   | 存储原因同药品  |
|`durgnum`|  检查次数|    |
|`repiceprice`|检查费用   | |


- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	返回个行数也行，不返回也成|
- data中的数据

```yaml
1
```
### 1.3.3 查询指定`Id`的病人已添加的药品之外的项目
- 业务说明：查询指定病人的、已经添加了的检查，就是本次诊断给病人添加的，药品之外的项目有哪些。
>注意：不是所有的项目都是需要检查的。比如`打针`这个项目只要收完费，在本系统里面的流转也就完成了。而`CT`这个项目是需要检查的，收完费之后，还要在`项目检查`这个菜单里面查询出来，并提交检查相关的信息
- 请求路径：`/hospital/caocc/selximu`
- 请求类型：`GET`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`perid`|病人的`id`||
|`mstate`|病人检查项目是否已检查|1:已检查 ，0:未检查。如果未传值应该返回所有记录|
|`page`|当前页码|不能为空|
|`limit`|每页条数|不能为空|

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	返回个行数也行，不返回也成|
- data中的数据

```yaml
{
  "msg": "",
  "code": 0,
  "data": [
    {
      "cashier": 58,
      "reportId": 35,
      "durgname": "ct",
      "durgnum": 1,
      "repiceprice": 100.0,
      "repicetotal": 100.0,
      "state": 1,
      "ostate": 1,
      "jie": null,
      "mstate": 0
    }
  ],
  "count": 1
}
```
## 1.4 项目缴费
### 1.4.1 查询指定ID的病人的待检查费用总计
- 业务说明：查询指定病人的、已经添加了的但是还没有执行的、还没有缴费的所有项目的费用总计金额，就是本次诊断给病人添加的，要做的检查待缴费的总金额是多少
- 请求路径：`/hospital/xpay/selshoux`
- 请求类型：`GET`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`perid`|病人的`id`||

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|直接放金额就可以了|
- data中的数据

```yaml
20
```
### 1.4.2 设置病人已完成检查项目的缴费
- 业务说明：原项目默认病人会一次性把所有检查项的费用全部缴上，所以只向后端提交一个`perid`，后端直接把该病人所有的未缴费检查项更新成已缴费
- 请求路径：`/hospital/xpay/shoufei`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`perid`|病人的`id`||

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|可以返回更新的行数，也可以不返回|
- data中的数据

```yaml
1
```

## 1.5 项目检查
### 1.5.1 查询指定ID的病人的待检查项目
- 业务说明：查询指定病人的、已经缴费了的、需要做检查的项目有哪些
- 请求路径：`/hospital/xpay/selcha`
- 请求类型：`GET`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`perid`|病人的`id`||
|`page`|当前页码|不能为空|
|`limit`|每页条数|不能为空|
- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据||
- data中的数据

```javascript
{
  "msg": "",
  "code": 0,
  "data": [
    {
      "cashier": 61,
      "reportId": 37,
      "durgname": "ct",
      "durgnum": 1,
      "repiceprice": 100.0,
      "repicetotal": 100.0,
      "state": 1,
      "ostate": 1,
      "jie": null,
      "mstate": 1
    }
  ],
  "count": 1
}
```
### 1.5.2 添加/更新检查结果到挂号单
- 业务说明：添加/更新病人的检查结果，如果之前已经存在，就更新，否则新增
- 请求路径：`/hospital/caocc/addbing`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`cashier`|这个应该是检查项目的id||
|`bing`|病人的检查结果||
|`reid`|挂号单的`id`||


- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	返回个行数也行，不返回也成|
- data中的数据

```yaml
1
```

## 1.6 药品缴费

### 1.6.1 查询指定ID的病人的需要缴费的药品列表
- 业务说明：查询指定病人的、已经添加了的、需要缴费的药品列表
- 请求路径：`/hospital/caocc/selpepi`
- 请求类型：`GET`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`perid`|病人的`id`||
|`page`|当前页码|不能为空|
|`limit`|每页条数|不能为空|
- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据||
- data中的数据

```javascript
{
  "msg": "",
  "code": 0,
  "data": [
    {
      "cashier": 59,
      "reportId": 37,
      "durgname": "板蓝根",
      "durgnum": 2,
      "repiceprice": 30.0,
      "repicetotal": 60.0,
      "state": 0,
      "ostate": null,
      "jie": null,
      "mstate": null
    }
  ],
  "count": 1
}
```
### 1.6.2 查询指定ID的病人的药品费用总计
- 业务说明：查询指定病人的药品的总费用
- 请求路径：`/hospital/caoout/selch`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`reportId`|挂号单`id`||

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|直接放金额就可以了|
- data中的数据

```yaml
60
```
### 1.6.3 设置病人已完成药品的缴费
- 业务说明：原项目默认病人会一次性把所有药品的费用全部缴上，所以只向后端提交一个`perid`，后端直接把该病人所有的未缴费药品更新成已缴费
- 请求路径：`/hospital/xpay/shoufei`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`reportId`|挂号单的`id`||

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|可以返回更新的行数，也可以不返回|
- data中的数据

```yaml
1
```

## 1.7 门诊取药
http://hi.tsyj.net/hospital/caotake/tselpreson?page=1&limit=5
### 1.7.1 查询所有待取药品的病人
- 业务说明：
- 请求路径：`/caotake/tselpreson`
- 请求类型：`GET`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`page`|当前页码|不能为空|
|`limit`|每页条数|不能为空|

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|	返回个行数也行，不返回也成|
- data中的数据

```javascript
{
  "msg": "",
  "code": 0,
  "data": [
    {
      "reportId": 37,
      "reportName": "王猛",
      "sex": "男",
      "age": 22,
      "price": 30.0,
      "time": "2021-12-25",
      "users": "超级管理员",
      "state": 2,
      "ddepartmentid": 7,
      "ddoctorid": 14,
      "dredisteredid": null,
      "department": "内科",
      "doctorName": "扁鹊",
      "type": "专家号",
      "carid": "371326199966664128",
      "phone": "13328567943",
      "carido": null,
      "caridt": null,
      "cc": null,
      "datime": null,
      "zhuan": null
    }
  ],
  "count": 1
}
```
### 1.7.2 设置病人已取走所有药品
- 业务说明：原项目默认病人会一次性把所有药品全部取走，后端需要在设置所该病人已经完成取药的同时对药品做出库操作【为后面的药品库存管理做准备】
- 请求路径：`/hospital/caotake/chuku`
- 请求类型：`POST`
- 请求参数：

|参数名称|参数说明|备注|
|----:|:------|:-----|
|`reportId`|挂号单的`id`||

- 响应数据 `SysResult`对象

|参数名称	|参数说明|	备注|
|---:|:---|:---|
|`status`	|状态信息	|200表示服务器请求成功 201表示服务器异常|
|`msg`	|服务器返回的提示信息	|可以为null|
|`data`	|服务器返回的业务数据|可以返回更新的行数，也可以不返回|
- data中的数据

```yaml
1
```